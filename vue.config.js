'use strict'
const path = require('path')
const defaultSettings = require('./src/settings.js')

function resolve(dir) {
  return path.join(__dirname, dir)
}

const name = defaultSettings.title || 'vue Admin Template' // page title

// 对项目的端口进行配置
// 第一种方案：直接去更改原先的端口
const port = process.env.port || process.env.npm_config_port || 9528 // dev port

// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {
  /**
   * You will need to set publicPath if you plan to deploy your site under a sub path,
   * for example GitHub Pages. If you plan to deploy your site to https://foo.github.io/bar/,
   * then publicPath should be set to "/bar/".
   * In most cases please use '/' !!!
   * Detail: https://cli.vuejs.org/config/#publicpath
   */
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    // 配置代理转发 proxy 全小写
    proxy: {
      // /api 是程序员自定义的一个字符串(可以是任意的)
      // 代表是请求的基准路径，如果请求的基准路径是 /api，才会执行代理转发
      // 这个 /api 是指设置的请求基准路径，所以需要将 VUE_APP_BASE_API 设置为 '/api'
      '/dev_api': {
        // target 是后端服务器(目标服务器、数据服务器)地址
        // 注意事项：请求的基准路径 /api，在触发代理转发的时候，会自动拼接到 target 地址后面
        // 也就是：http://8.131.91.46:6868/api
        // 所以需要将接口地址前面的 /api 去掉才可以，否则 http://8.131.91.46:6868/api/api 会多一个 /api
        target: 'http://ihrm.itheima.net',

        // 是否跨域请求，一般设置为 true 即可，只要设置为 true，才是真正的跨域请求
        changeOrigin: true,

        // 项目中接口的请求前缀是 /api，但是目前我们将请求的基准路径换成了 /dev_api
        // 那么 /dev_api 就会拼接到 url 地址后面：http://8.131.91.46:6868/dev_api
        // 这个拼接以后，请求地址是错误的，我们需要把 /dev_api 再次改成 /api 才可以，这时候就用到了【路径重写】
        pathRewrite: {
          // 键值形式配置
          // 键：当前请求基准路径，就是对谁进行重写，值：需要替换的值
          '^/dev_api': '/api' // 配置好以后，就把路径重新改为了 http://8.131.91.46:6868/api
        }
      }
    }

    // 这个字段是用来设置 Mock，(伪造数据的，因为咱们有接口，不需要伪造)
    // Mock 怎么使用，Vue 3 会讲到
    // before: require('./mock/mock-server.js')
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  chainWebpack(config) {
    // it can improve the speed of the first screen, it is recommended to turn on preload
    config.plugin('preload').tap(() => [
      {
        rel: 'preload',
        // to ignore runtime.js
        // https://github.com/vuejs/vue-cli/blob/dev/packages/@vue/cli-service/lib/config/app.js#L171
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }
    ])

    // when there are many pages, it will cause too many meaningless requests
    config.plugins.delete('prefetch')

    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    config
      .when(process.env.NODE_ENV !== 'development',
        config => {
          config
            .plugin('ScriptExtHtmlWebpackPlugin')
            .after('html')
            .use('script-ext-html-webpack-plugin', [{
            // `runtime` must same as runtimeChunk name. default is `runtime`
              inline: /runtime\..*\.js$/
            }])
            .end()
          config
            .optimization.splitChunks({
              chunks: 'all',
              cacheGroups: {
                libs: {
                  name: 'chunk-libs',
                  test: /[\\/]node_modules[\\/]/,
                  priority: 10,
                  chunks: 'initial' // only package third parties that are initially dependent
                },
                elementUI: {
                  name: 'chunk-elementUI', // split elementUI into a single package
                  priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                  test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
                },
                commons: {
                  name: 'chunk-commons',
                  test: resolve('src/components'), // can customize your rules
                  minChunks: 3, //  minimum common number
                  priority: 5,
                  reuseExistingChunk: true
                }
              }
            })
          // https:// webpack.js.org/configuration/optimization/#optimizationruntimechunk
          config.optimization.runtimeChunk('single')
        }
      )
  }
}
