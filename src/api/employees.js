import request from '@/utils/request'

// 获取-员工列表
export function getEmployeesListAPI(params) {
  return request({
    url: '/sys/user',
    params
  })
}

// 删除-员工
export function delEmployeesAPI(id) {
  return request({
    url: `/sys/user/${id}`,
    method: 'DELETE'
  })
}

// 新增员工
export function addEmployeesAPI(data) {
  return request({
    url: '/sys/user',
    method: 'POST',
    data
  })
}

// 批量-新增员工(以前发送对象JSON字符串, 所以有参数名要求(key名跟后台一致))
// 直接把数组发送给后台
export function importEmployee(data) {
  return request({
    url: '/sys/user/batch',
    method: 'POST',
    data
  })
}

/**
 * @description: 获取员工详情
 * @param {*} id 用户id
 * @return {*}
 */
export function getUserDetailByIdAPI(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

// RestFul API风格的接口
export function saveUserDetailByIdAPI(data) {
  return request({
    url: `/sys/user/${data.id}`,
    method: 'PUT',
    data
  })
}
