import request from '@/utils/request'

/**
 * 登录功能
 * @param { mobile, password } data 手机号，密码
 * @returns
 */
export function login(data) {
  return request({
    url: '/sys/login',
    method: 'POST',
    data
  })
}

/**
 * @description: 获取用户资料
 * @param {*}
 * @return {*}
 */
export function getUserProfile() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

/**
 * @description 获取用户的基本信息
 * @param { string } id 用户的 id
 * @returns
 */
export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`,
    method: 'GET'
  })
}

export function logout() {
}
