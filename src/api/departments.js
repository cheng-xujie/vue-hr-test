import request from '@/utils/request'

// 获取-所有部门列表
export function getDepartmentsListAPI() {
  return request({
    url: '/company/department',
    method: 'GET'
  })
}

// 获取-员工简单列表
export function getEmployeesSimpleListAPI() {
  return request({
    url: '/sys/user/simple'
  })
}

// 新增-组织部门
export function createDepartmentsAPI(data) {
  return request({
    url: '/company/department',
    method: 'POST',
    // params: {}, // 这个里面key名和值, 会被axios拼接在?后面发送给后台 (也叫query, 查询字符串)
    data // 这个里面key名和值, 会被axios放在请求体里携带给后台 (也叫body, 请求体)
  })
}

// 根据部门ID->获取部门详情
export function getDepartDetailByIdAPI(id) {
  return request({
    url: `/company/department/${id}`
  })
}

// 根据部门ID->修改部门详情
export function updateDepartDetailByIdAPI(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'PUT',
    data
  })
}

// 根据部门ID->删除部门
export function delDepartAPI(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'DELETE'
  })
}
