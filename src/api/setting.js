import request from '@/utils/request'

// 获取角色列表
export function getRoleListAPI(params) {
  return request({
    url: '/sys/role',
    method: 'GET',
    params
  })
}

// 根据公司ID -> 获取公司信息
export function getCompanyInfoAPI(companyId) {
  return request({
    url: `/company/${companyId}`
  })
}

// 新增角色
export function addRoleAPI(data) {
  return request({
    url: '/sys/role',
    method: 'POST',
    data
  })
}

// 获取角色->详情
export function getRoleDetailByIdAPI(roleId) {
  return request({
    url: `/sys/role/${roleId}`
  })
}

// 根据ID->更新角色详情
export function updateRoleDetailByIdAPI(data) {
  return request({
    url: `/sys/role/${data.id}`,
    method: 'PUT',
    data
  })
}

// 根据ID->删除角色
export function delRoleAPI(roleId) {
  return request({
    url: `/sys/role/${roleId}`,
    method: 'DELETE'
  })
}
