/**
 * auth 这个模块的主要作用就是用来操作 token
 */

// js-cookie 就是将数据存储到 cookie 中
import Cookies from 'js-cookie'

const TokenKey = 'vue-hr'

// 获取 Token
export function getToken() {
  // localStorage.getItem('key')
  return Cookies.get(TokenKey)
}

// 设置 Token
export function setToken(token) {
  // localStorage.setItem(key, 真正需要存储的数据)
  return Cookies.set(TokenKey, token)
}

// 移除 token
export function removeToken() {
  return Cookies.remove(TokenKey)
}
