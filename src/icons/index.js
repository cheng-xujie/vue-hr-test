import Vue from 'vue'
// 组件就是用来展示 icon 图标的
import SvgIcon from '@/components/SvgIcon'// svg component

// register globally 全局注册
// Vue.component，
// 第一个参数：组件的名称
// 第二个参数：组件的具体逻辑代码
Vue.component('svg-icon', SvgIcon)

// 下面 三句话，不用记忆，因为下面的代码是 webpack 代码，是给 Vue-cli 看的
// 找到当前目录下的 svg 文件夹，找到里面后缀名为 .svg 的图标
// 使用 webpack 对 svg 图标进行解析，解析以后交给组件进行使用
const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)

// 如果以后我们项目中使用了 svg 图标，应该放到哪一个文件夹里面呢 ？
