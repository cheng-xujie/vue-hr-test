import PageTools from './PageTools'
import UploadExcel from './UploadExcel'
import UploadImg from './UploadImg'
// Vue.use()里传入的对象, 必须有install方法(Vue.use方法内的源码会尝试调用install方法)
export default {
  install(Vue) {
    // 执行一些代码(将来再注册其他的全局组件都可以写到这里)
    // main.js, 只需要写一行Vue.use()即可
    Vue.component('PageTools', PageTools)
    Vue.component('UploadExcel', UploadExcel) // 上传表格的组件
    Vue.component('UploadImg', UploadImg) // 上传头像的Upload组件
  }
}
// 导出插件对象
// export default function(Vue) {
//   Vue.component('PageTools', PageTools)
// }
