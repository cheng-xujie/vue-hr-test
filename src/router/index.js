import Vue from 'vue'
import Router from 'vue-router'

// 将 VueRouter 挂载为 Vue 的插件
Vue.use(Router)

/* 布局容器：包含了项目最基础的结构 */
// @ 符号代表的 src 目录
import Layout from '@/layout'

// 动态路由表(为后面做准备-后台返回权限信息筛选的事情最后2天讲)
// 先把8个路由页面先配置好, 都显示出来
import approvalsRouter from './modules/approvals'
import departmentsRouter from './modules/departments'
import employeesRouter from './modules/employees'
import permissionRouter from './modules/permission'
import attendancesRouter from './modules/attendances'
import salarysRouter from './modules/salarys'
import settingRouter from './modules/setting'
import socialRouter from './modules/social'
export const asyncRoutes = [
  departmentsRouter, // 组织架构(公司部门关系)
  settingRouter, // 角色管理
  employeesRouter, // 员工管理
  permissionRouter, // 权限管理(权限点)
  approvalsRouter,
  attendancesRouter,
  salarysRouter,
  socialRouter
]

// constantRoutes 数组中是定义好的路由规则(静态路由表-大家都能用)
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true // 不生成左侧菜单
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true // 不生成左侧菜单
  },

  {
    path: '/', // 一般是代表默认路由
    component: Layout, // 当访问 / 时，展示的是 Layout 布局容器组件
    redirect: '/dashboard', // 重定向
    meta: { title: '首页123', icon: 'dashboard' },
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
    // alwaysShow: true
  },

  {
    path: '/excel',
    component: Layout,
    hidden: true, // 导入页面只需要点击跳转,无需左侧导航
    children: [{
      path: '',
      name: 'excel',
      component: () => import('@/views/excel/index.vue')
    }]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

// 对 VueRouter 进行实例化
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRoutes, ...asyncRoutes] // 配置路由规则 [{}, {}, {}, {}, {}]
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
