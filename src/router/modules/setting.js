import Layout from '@/layout'
export default {
  path: '/setting',
  component: Layout,
  children: [
    {
      path: '',
      name: 'setting',
      component: () => import('@/views/setting/index.vue'),
      meta: { title: '角色设置', icon: 'setting' }
    }
  ]
}
