import Layout from '@/layout'
export default {
  path: '/employees',
  component: Layout,
  children: [
    {
      path: '', // 这里不是空格, 而是空字符串, 默认匹配的二级路由(地址栏地址/employees后面没了, 默认匹配一次空字符串path路径)
      name: 'employees', // 路由跳转this.$router.push({ path: '/employees' })
      // this.$router.push({ name: 'employees' })
      // name可以不动, 但是path可能会修改 (name就使用来路由跳转的)
      component: () => import('@/views/employees/index.vue'),
      // 路由对象里配置额外信息
      meta: { title: '员工', icon: 'people' }
    },
    {
      path: 'detail',
      name: 'employeesDetail',
      component: () => import('@/views/employees/detail.vue'),
      hidden: true // 保证员工在左侧菜单是一层的渲染
    }
  ]
}
