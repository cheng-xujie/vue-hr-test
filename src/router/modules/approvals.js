import Layout from '@/layout'
// 每个js文件都是一个模块
// 想要被外部人使用的, 必须导出
export default {
  path: '/approvals',
  component: Layout,
  children: [
    {
      path: '',
      name: 'approvals',
      component: () => import('@/views/approvals/index.vue'),
      meta: { title: '审批', icon: 'tree-table' }
    }
  ]
}
