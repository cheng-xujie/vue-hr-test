module.exports = {

  /**
   * title：页面的标题
   * 这个 settings.js 不是配置项，为什么还要进行重启项目
   * 因为这个 title 文件是在配置项中使用的，就是配置项引入了这个 title 名字
   */
  title: '人力资源管理平台',

  /**
   * 固定头部：头部区域默认没有固定
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: true,

  /**
   * 是否在侧边栏区域展示项目的 logo
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true
}
