// 导入操作 Token 的方法
import { getToken, setToken, removeToken } from '@/utils/auth'
// 导入接口
import { login, getUserProfile, getUserDetailById } from '@/api/user'

const state = () => ({
  // 尝试从本地获取 token，如果没有，就位空字符串 ''
  token: getToken() || '', // 登录以后，返回用户标识 token
  userInfo: {} // 用户信息
})

const mutations = {
  // 设置 token
  setToken(state, token) {
    state.token = token
    // 将 Token 存储到本地
    setToken(token)
  },

  // 移除 token
  removeToken(state) {
    state.token = ''
    // 移除 token 的时候，也需要从本地进行移除
    removeToken()
  },

  // 设置用户信息
  setUserInfo(state, userInfo) {
    state.userInfo = userInfo
  },

  // 移除用户信息
  removeUserInfo(state) {
    state.userInfo = {}
  }
}

const actions = {
  // 登录的逻辑
  async loginAction(context, data) {
    // 1. 调用接口，获取数据(token)
    const res = await login(data)
    // 2. 调用 mutation，将 token 存储到 state 中
    context.commit('setToken', res.data)
  },

  // 获取用户信息的 action 方法
  async getUserInfo(context) {
    // 发起请求，获取数据
    const res = await getUserProfile()
    const userInfo = await getUserDetailById(res.data.userId)

    // 获取的都是用户信息，所以需要都存放到 userInfo 中
    context.commit('setUserInfo', { ...res.data, ...userInfo.data })
  },

  // 退出的 action
  logout(context) {
    // 清除 token
    context.commit('removeToken')
    // 清除用户信息
    context.commit('removeUserInfo')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
