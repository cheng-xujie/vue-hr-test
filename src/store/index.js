import Vue from 'vue'
import Vuex from 'vuex'

// 导入提取的 getter 模块
import getters from './getters'

// 下面三个是导入的功能模块
// 下面三个模块其中 app、settings 是框架基本的数据，是完整的、固定死的
// 不要进行任何修改
import app from './modules/app'
import settings from './modules/settings'

// user 模块是我们自己的开发模块，这个模块是我们可以进行修改的
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user
  },
  getters
})

export default store
