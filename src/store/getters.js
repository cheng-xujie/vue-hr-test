const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  usernName: state => state.user.userInfo.username,
  staffPhoto: state => state.user.userInfo.staffPhoto,
  // 公司ID
  companyId: state => state.user.userInfo.companyId
}
export default getters
