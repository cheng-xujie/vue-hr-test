// 导入路由
import router from './router'
// 导入 store 模块
import store from './store'

// 导入进度条插件以及样式
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import getPageTitle from '@/utils/get-page-title' // 拿到拼接浏览器标题的方法

// 声明白名单
const whiteList = ['/login', '/404']

// 路由的前置守卫
router.beforeEach(async(to, from, next) => {
  // 路由发生页面切换时, 切换浏览器标签栏上标题
  // document.title = "内容"
  // 前置名字->在settings.js中定义的(代码规范要求:不要在代码中出现固定的值)
  // 为什么: 以后不方便维护和修改 (封装成全局的配置放到settings.js)
  document.title = getPageTitle(to.meta.title)

  // 当访问某一个路由的时候，需要直接显示进度条
  NProgress.start()

  // 获取 token
  const token = store.state.user.token

  // 判断 token 是否存在
  if (token) {
    // 判断用户访问的是否是登录页面
    if (to.path === '/login') {
      // 如果有 token ，说明登录了，访问到又是登录页面，直接跳转到主页即可
      // 不需要用户进行重复登录操作
      next('/')
    } else {
      // 在放行之前，先行获取用户新的数据，在获取到数据以后，在进行跳转
      // 为什么需要在这里调用呢？
      // 为了后面做权限管理功能做铺垫

      // dispatch 是异步的
      // 问题: 为何获取用户信息要在路由守卫里写, 为何不能写在首页.vue文件内?
      // 答案: 网页打开的时候, 用户可能直接看到的是首页, 也可能看到的直接就是员工页面/其他页面
      await store.dispatch('user/getUserInfo')
      next()
    }
  } else {
    // 判断访问的地址是否在白名单中
    // includes 用来判断数组中是否存在某个字符串，如果存在，返回 true，否则返回 false
    if (whiteList.includes(to.path)) {
      next()
    } else {
      // 如果访问的页面不在白名单中
      next('/login')
    }
  }

  // 强制关闭进度条，解决一些特殊、极端情况下，进度条不关闭的问题(例如：退出出错，进度条不关闭)
  NProgress.done()
})

// 路由的后置守卫
router.afterEach((to, from) => {
  // 进入路由以后，需要隐藏进度条
  NProgress.done()
})
